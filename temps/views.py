
from django.shortcuts import render, redirect
from .models import Weather
from datetime import datetime
import requests

def weather_list(request):
    weather_data = Weather.objects.all()
    return render(request, 'weather.html', {'weather_data': weather_data})

def get_weather(request):
    if request.method == 'POST':
        city = request.POST['city']
        
        # Utilisez l'API météo (par exemple, OpenWeatherMap) pour obtenir les données météorologiques.
        # Remplacez 'YOUR_API_KEY' par votre clé API.
        YOUR_API_KEY = '48bd6d99d1725f5c6bc5cb680951129f'
        api_url = f'http://api.openweathermap.org/data/2.5/weather?q={city},TG&appid={YOUR_API_KEY}'

        response = requests.get(api_url)
        data = response.json()
        temp = data['main']['temp']
        print(data)
        print("______________________")
        print(city)
        print(data['dt'])
        print(data['main']['temp'])
        print("--")
        print((temp-32)*5 / )
        print(data['main']['humidity'])
        print(data['weather'][0]['description'])
        print("*********************")

        if response.status_code == 200:
            # Enregistrez les données dans la base de données.
            """ temps = Weather()
            temps.ville = city
            temps.date = data['dt']
            temps.temperature = data['main']['temp']
            temps.humidite=data['main']['humidity']
            temps.description=data['weather'][0]['description'] """
            
            Weather.objects.create(
                ville=city,
                date=datetime.fromtimestamp(data['dt']),
                temperature=(data['main']['temp']-32)*5/9,
                humidite=data['main']['humidity'],
                description=data['weather'][0]['description']
            )
            
            return redirect('weather_list')
    
    return render(request, 'weather.html')