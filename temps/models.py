# 48bd6d99d1725f5c6bc5cb680951129f
from django.db import models

class Weather(models.Model):
    ville = models.CharField(max_length=100)
    date = models.DateTimeField()
    temperature = models.DecimalField(max_digits=5, decimal_places=2)
    humidite = models.DecimalField(max_digits=5, decimal_places=2)
    description = models.TextField()

    def __str__(self):
        return f"Weather in {self.ville} on {self.date}"